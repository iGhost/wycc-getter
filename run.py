#!/usr/bin/env python
# -*- coding: utf-8 -*-

# TODO:
# * Convert timezone from Youtube (Pacific Time) to My, but do we really need this?
# http://stackoverflow.com/questions/16603645/how-to-convert-datetime-time-from-utc-to-different-timezone
# http://stackoverflow.com/questions/10997577/python-timezone-conversion
#
# * Check best format when downloading video
# * Track download process


import pynotify
import feedparser
from os import system

videos_file = 'wycc.list'
rss_url = 'https://www.youtube.com/feeds/videos.xml?channel_id=UCUZLQoU0DDMhO5CNIUStfug'


def send_message(title, message):
    pynotify.init("Wycc Getter")
    notice = pynotify.Notification(title, message)
    notice.show()
    return


def save_videos(videos):
    f = open(videos_file, 'w')
    f.writelines([v+"\n" for v in videos])
    f.close()


def load_videos():
    videos = open(videos_file).readlines()
    return [v.strip("\n") for v in videos]


def download_video(video):
        send_message('Download started', video.title)
        system("youtube-dl -f 137+140 --prefer-ffmpeg {}".format(video.yt_videoid))


def main():
    d = feedparser.parse(rss_url)

    saved_videos = load_videos()
    feed_videos = [e.yt_videoid for e in d.entries]
    new_videos = list(set(feed_videos) - set(saved_videos))

    if new_videos:
        send_message('Wycc has just published new video{}'.format('s' if len(new_videos) > 1 else ''),
                     "\n".join([v.title for v in d.entries if v.yt_videoid in new_videos]))
        download_video([v for v in d.entries if v.yt_videoid == new_videos[0]][0])

    save_videos(feed_videos)


if __name__ == "__main__":
    main()
